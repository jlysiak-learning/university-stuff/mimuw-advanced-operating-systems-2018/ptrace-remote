# ZSO 2017/18
## Zadanie 3 - ptrace remote

Jacek Łysiak

Rozwiązanie jest w postaci patcha na wersję 4.9.13 (commit: 3737a5f7223464efed3b0a9da2b045dae28d3a53)
Użyte źródło jądra: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/commit/?h=v4.9.13

Patch znajduje się w pliku `zso-zad3-jl345639.patch`.

### OPIS

Rozszerzenia `REMOTE` zostały dodane do ptrace'a w `prace_request`,
poprzez wstawienie case'ów dla wartości request'u z `ptrace_remote.h`.
Dzięki temu wykorzystywana jest cała "infrastruktura" standardowej implementacji
ptrace. Zapewnia ona, że warunki zadania będą pełnione (kody błędów, wstrzymanie procesu, etc.).
Dla odpowiednich case'ów wołane są metody, które kopiują struktury z argumentami z userspace'u.

W pierwszej kolejności zaimplementowane zostały funkcje operujące
na deskryptorach zdalnego procesu `fs/file.c`
Odpowiednie implementacje remote dup i dup3 były wzorowane na ich standardowych odpowiednikach.
Modyfikacje polegają na tym, że nowe implementacje pobierają `struct files_struct`
procesu śledzonego i musimy przekazać ją do `do_dup2` oraz `__install_fd` (w zależności od funkcji).
Należy zapewnić odpowiednią synchronizację wykorzystując spin_lock'i, gdyż 
operujemy na dwóch różnych strukturach files_struct - procesu śledzącego i śledzonego.
Na czas wykonywania operacji remote dup* zwiększamy tymczasowo licznik referencji, aby
nie doszło do zamknięcia pliku.

Aby zrealizować operacje na pamięci wykorzystano ideę PTRACE_SINGLESTEP, 
aby wybudzić tracee na chwilę tylko po to, żeby mógł wywołać syscall w swojej przestrzeni
adresowej.

Tracer w swojej części `do_ptrace_remote_in_tracer_ctx` sprawdza argumenty,
inicjalizuje `struct completion`, aby móc czekać na wykonanie syscalla.
Następnie wstawia argumenty i pointer na `struct completion` do task structa 
procesu śledzonego, gdzie zostały dodane odpowiednie pola i czeka na wait_for_completion.

Została dodana flaga TIF_PTRACE_REMOTE i w `exit_to_usermode_loop` sprawdzamy,
czy tracer nie ustawił jej w procesie śledzonym.
Jeśli tak, wykonujemy kod z `do_ptrace_remote_in_tracee_ctx`.
Czytamy argumenty i request przygotowane przez proces śledzący.
Wołamy odpowiedniego syscalla i zwracamy wynik, przekazując go do task structa
procesu śledzącego.
Następnie tracee usypia się, ustawiwszy wcześniej swój stan na TASK_TRACED.

